###############################################################################
# Tute 9 - Example script from tute-9                                         #
# Example Script given in tute 9                                              #
# Modified by - Cameron Watt                                                  #
###############################################################################
#bin/bash

while getopts ":abc:d:" opt; do
  case $opt in
    a)
      echo "-a was triggered!" >&2
      ;;
    b)
      echo "-b was triggered!" >&2
      ;;
    c)
      echo "-c was triggered!" >&2
      echo $OPTARG
      ;;
    d)
     echo "-d was triggered!" >&2
     echo $OPTARG
     ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
#NOTES  ./go_opt.sh -a -b   or -ab  will successfully trigger a or b
#       -ab hello -a   WILL not trigger the final -a, as get_opt stops when it detects hello and cant assign it to an argument
#       -c hello -d hello -ab   WILL execut ALL arguments  


initial_options(){
while getopts ":a" opt; do
  case $opt in
    a)
      echo "-a was triggered!" >&2
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
}
